# CarCar

Team:

* Ray Khan - microservice: Service
* Ovidiu Buta - microservice: Sales

At the top of the webpage, the navigation bar includes drop downs for Sales, Inventory, and Service. Within each component, the user can explore each portion of the app and see the different forms we have included.

Sales - enables the user to:
1: check sales record and view sales history,
2: add new customer,
3: add sales rep,
4: record a new sale,
5: filter sales completed based on sales rep.

Inventory - enables the user to:
1: create new manufacturer,
2: create new model,
3: create new automobile as it comes into inventory,
4: view manufactuere list,
5: view all vehicle models,
6: view autmobiles list (all cars stores in inventory).

Service - enables the user to:
1: view all appointments on the bulletin board,
2: create new appointment,
3: add new technician,
4: filter appointments based on VIN.


Design approach based on the microservices -

Sales - The sales microservice is based on 4 models: AutomobileVO (value obj), SalesRep (entity), Customer (entity), and SalesRecord (entity). The AutombileVO model is used to poll the data from the Inventory microservice. The sales record is generated from the data in the sales and inventory microservice, and this can be used to show sales records (which includes sales rep name, employee number, customer name, vin, and sales price). Additionally, we added the functionality to filter the sales records based on the sales rep's name. Once a sale is complete, that vehicle's VIN can no longer be selected in the drop down for sales, since it is sold and no longer in the inventory.

Service - The Service microservice is based on 3 models: AutomobileVO (value obj), Tech (entity), and Service Appointment (entity). This microservice polls data from the inventory microservice through the AutomobileVO. The VIN is important because if the VIN input in the service matches the VIN in Inventory, this will result in that vehicle being marked as VIP treatment, since it belonged to the dealership initially (essentially, repeat customer repeat business). Overall, the Service microservice allows the user to add appointments to the bulletin board (a to-do list), display all appointments, and check appointment history, including filtering based on VIN. Once the job is complete, the user can select Finished button. Alternatively, if the appointment is cancelled, the user can go on the webpage and press cancelled. In either case, the appointment is then removed. Additionally, there is the capabiity to add a new technician.
