function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">Vermandois Autos</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
        Le plus exclusiviste auto émotion
        </p>
      </div>
      <a href="https://static.palais.mc/medias_upload/vos_fichiers/CollectionVoitures/2442.jpg" target="_blank"><img src="https://static.palais.mc/medias_upload/vos_fichiers/CollectionVoitures/2442.jpg" alt="image host"/></a>
    </div>
  );
}

export default MainPage;
