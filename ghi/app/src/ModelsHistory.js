import React from "react";

class ModelList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            model_records: []
        };
    }
    
     //endpoints
     static salesRecordUrl = 'http://localhost:8100/api/models/';

     async componentDidMount() {
         const response = await fetch('http://localhost:8100/api/models/');
         if (response.ok) {
             const data = await response.json()
             this.setState({ model_records: data.models });
         }
     }


    render(){
        const image_width = {
            width: '70%'
          };
        return (
            <> 
            <h1>Model List</h1>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Model</th>
                <th>Manufacturer</th>
                <th>Picture</th>
            </tr>
            </thead>
            <tbody>
            {this.state.model_records ? this.state.model_records.map(_model => {
                            return (
                                <tr key={_model.id}>
                                    <td>{_model.name}</td>
                                    <td>{_model.manufacturer.name}</td>
                                    <td>{<img style={image_width} src={_model.picture_url}/>}</td>
                                </tr>
                            );
                        }):null}
            </tbody>
        </table>
            </> 
        );
    };
}

export default ModelList;