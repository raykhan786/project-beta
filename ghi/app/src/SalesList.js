import React from "react";

class SalesList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sales_records: []
        };
    }

    //endpoints
    static salesRecordUrl = 'http://localhost:8090/api/salesrecord/';

    async componentDidMount() {
        const response = await fetch('http://localhost:8090/api/salesrecord/');
        if (response.ok) {
            const data = await response.json()
            this.setState({ sales_records: data.all_sales_records });
        }
    }

    render() {
        return (
            <>
                <h1>List of All Sales</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales Representative</th>
                            <th>Employee Number</th>
                            <th>Customer</th>
                            <th>Automobile VIN</th>
                            <th>Sales Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.sales_records ? this.state.sales_records.map(_sale => {
                            return (
                                <tr key={_sale.id}>
                                    <td>{_sale.sales_rep.name}</td>
                                    <td>{_sale.sales_rep.employee_number}</td>
                                    <td>{_sale.customer}</td>
                                    <td>{_sale.automobile.vin}</td>
                                    <td>${_sale.price}</td>
                                </tr>
                            );
                        }):null}
                    </tbody>
                </table>
            </>
        )
    }
}

export default SalesList;
