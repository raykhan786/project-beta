import React from 'react'

class TechList extends React.Component {
constructor(props) {
    super(props)
    this.state = {
    technicians: []
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
}

async handleSubmit(event) {
    event.preventDefault()
    const data = { ...this.state }
    delete data.technicians
    console.log(data)

    const conferenceUrl = 'http://localhost:8080/api/ApptList/'
    const fetchConfig = {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
        'Content-Type': 'application/json',
    },
    }
    const response = await fetch(conferenceUrl, fetchConfig)
    if (response.ok) {
    const ServiceForm = await response.json()
    //  console.log(ServiceForm)   (dont think we need it)

    window.location.reload()

    }
}

handleChange(event) {
    this.setState({
    [event.target.name]: event.target.value
    });
}

async componentDidMount() {
    const url = 'http://localhost:8080/api/techs/'

    const response = await fetch(url)

    if (response.ok) {
    const data = await response.json()
    this.setState({ technicians: data.technicians })
    }
}

render() {
    return (  <div className='mb-3'>
        <h1>List of Technicians</h1>
        <select value={this.state.technician} required id="technician" name="technician" className="form-select">
                {this.state.technicians.map(technician => {
                    return (
                    <option key={technician.id} value={technician.name}>
                        {technician.name}
                    </option>
                    )
                })}
                </select>
    </div>
    )
    }
}


export default TechList
