from django.contrib import admin
from .models import AutomobileVO, SalesRep, Customer, SalesRecord


admin.site.register(AutomobileVO)
admin.site.register(SalesRep)
admin.site.register(Customer)
admin.site.register(SalesRecord)
