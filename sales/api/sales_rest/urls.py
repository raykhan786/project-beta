from django.urls import path
from .views import api_list_customers, api_list_sales_records, api_list_sales_reps, api_show_sale_records


urlpatterns = [
    path("customers/", api_list_customers, name="create_customer"),
    path("salesrecord/<int:pk>/", api_show_sale_records, name="edit_sales_record"),
    path("salesreps/", api_list_sales_reps, name="create_sales_reps"),
    path("salesrecord/", api_list_sales_records, name="create_sales_record"),
]
