from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
import requests
from .models import AutomobileVO, SalesRep, SalesRecord, Customer
from .encoders import SalesRecordEncoder, SalesRepEncoder, CustomerEncoder


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder = CustomerEncoder
        )

    else:
        try:
            content = json.loads(request.body)
            print(content)

            new_customer = Customer.objects.create(**content)
            return JsonResponse(
                new_customer,
                encoder = CustomerEncoder,
                safe = False,
            )
        except:
            response = JsonResponse({"Message": "Cannot create a customer. Error"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_list_sales_records(request):
    if request.method == "GET":
        sales_records = SalesRecord.objects.all()
        return JsonResponse(
            {"all_sales_records": sales_records},
            encoder = SalesRecordEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            automobile_vin = content["automobile"]  # Automobile object app Inventory
            response = requests.get('http://inventory-api:8000/api/automobiles/' + automobile_vin + '/')

            automobile_obj = json.loads(response.content)
            print(automobile_obj)

            automobile = AutomobileVO(vin=automobile_obj['vin'], year=automobile_obj['year'], sold=True, import_href=automobile_obj['href'], color=automobile_obj['color'])
            automobile.save()

            content["automobile"] = automobile

            sales_rep_id = content["sales_rep"]
            sales_rep = SalesRep.objects.get(id=sales_rep_id)
            content["sales_rep"] = sales_rep


            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer

            print(content)

            new_sales_record = SalesRecord.objects.create(**content)
            return JsonResponse(
                new_sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )



        except Exception as e:
            response = JsonResponse ({"Message": e})
            response.status_code = 400
            return response



@require_http_methods(["GET", "POST"])
def api_list_sales_reps(request):
    if request.method == "GET":
        sales_reps = SalesRep.objects.all()
        return JsonResponse(
            {"sales_reps": sales_reps},
            encoder = SalesRepEncoder,
        )

    else:
        try:
            content = json.loads(request.body)
            print(content)

            new_sales_reps = SalesRep.objects.create(**content)
            return JsonResponse(
                new_sales_reps,
                encoder = SalesRepEncoder,
                safe = False,
            )
        except:
            response = JsonResponse({"Message": "Cannot create a sales rep. Error"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET"])
def api_show_sale_records(request, pk):
    if request.method == "GET":
        try:
            sale_record = SalesRecord.objects.get(id=pk)
            return JsonResponse(
                sale_record,
                encoder = SalesRecordEncoder,
                safe = False,
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"Message": "No sales record here. Error"})
            response.status_code = 400
            return response

    else:
        count, _ = SalesRecord.objects.filter(id=pk).delete()
        return JsonResponse({"Deleted": count > 0})
